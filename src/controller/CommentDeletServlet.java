package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.DeletService;

@WebServlet(urlPatterns = { "/commentDelet" })
public class CommentDeletServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		int commentId = Integer.parseInt(request.getParameter("comment_id"));

		new DeletService().commentDelet(commentId);

		response.sendRedirect("home");
    }

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

	}
}
