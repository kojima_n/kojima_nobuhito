package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.UesrManagementService;
import service.UserService;
import service.signupService;

@WebServlet(urlPatterns = { "/userEdit" })
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branches = new signupService().getBranch();
		request.setAttribute("branches", branches);

		List<Department> departments = new signupService().getDepartment();
		request.setAttribute("departments", departments);

		List<User> user = new UesrManagementService().getUser();
		request.setAttribute("user", user);

		//HttpSession session = request.getSession();
		//String editUserId = (String)session.getAttribute("editUserId");
		String editUserId = request.getQueryString();
		//System.out.println("UE:" + editUserId);
        User editUser = new UserService().getUser(Integer.parseInt(editUserId));
        //System.out.println("UE:" + editUser.getBranch_name());
        //System.out.println("UE:" + editUser.getDepartment_name());
        request.setAttribute("editUser", editUser);

		request.getRequestDispatcher("UserEdit.jsp").forward(request, response);
	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> messages = new ArrayList<>();
    	request.setCharacterEncoding("UTF-8");
    	HttpSession session = request.getSession();
    	User editUser = getEditUser(request);
    	String password = request.getParameter("password");

        if(isValid(request, messages) == true) {
        	if(password == "") {
        		new UesrManagementService().notPwUpdate(editUser);
            	response.sendRedirect("userManagement");
        	}else {
        		new UesrManagementService().update(editUser);
            	response.sendRedirect("userManagement");
        	}
        }else {
        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("userEdit");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String userId = request.getParameter("user_id");
        String password = request.getParameter("password");
        String passwordCheck = request.getParameter("password_chack");
        String userName  = request.getParameter("user_name");


        if(StringUtils.isEmpty(userId) == true) {
        	messages.add("ユーザーIDを入力してください");
        }else if(!userId.matches("^[0-9a-zA-Z]{6,20}$")) {
        	messages.add("ユーザーIDは半角英数字で6文字以上20文字以下で入力してください");
        }
        if(StringUtils.isEmpty(password) == false) {
        	if(!password.matches("^[ -/:-@\\[-~]{6,20}$")) {
            	messages.add("パスワードは半角英数字(記号含む)で6文字以上20文字以下で入力してください");
            }else if(!password.equals(passwordCheck)) {
            	messages.add("パスワードとパスワード(確認用)の入力内容が違います。同じ内容を入力してください");
            }
        }
        if(StringUtils.isEmpty(userName) == true) {
        	messages.add("ユーザー名を入力してください");
        }else if(!userName.matches("^.{1,10}$")) {
        	messages.add("ユーザー名は10文字以下で入力してください");
        }


        if(messages.size() == 0) {
        	return true;
        }else {
        	return false;
        }
    }

    private User getEditUser(HttpServletRequest request) throws IOException, ServletException {

    	User editUser = new User();
    	editUser.setId(Integer.parseInt(request.getParameter("id")));
    	editUser.setUser_id(request.getParameter("user_id"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setUser_name(request.getParameter("user_name"));
        editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

        return editUser;
    }

}
