package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.UserService;
import service.signupService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branches = new signupService().getBranch();
		request.setAttribute("branches", branches);

		List<Department> departments = new signupService().getDepartment();
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> messages = new ArrayList<>();

    	request.setCharacterEncoding("UTF-8");

    	HttpSession session = request.getSession();
        if(isValid(request, messages) == true) {
    		User user = new User();
            user.setUser_id(request.getParameter("user_id"));
            user.setPassword(request.getParameter("password"));
            user.setUser_name(request.getParameter("user_name"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
            user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
            user.setAccount_suspended(Integer.parseInt(request.getParameter("account_suspended")));

            new UserService().register(user);

            response.sendRedirect("userManagement");
        }else {
        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String userId = request.getParameter("user_id");
        String password = request.getParameter("password");
        String passwordCheck = request.getParameter("password_chack");
        String userName  = request.getParameter("user_name");
        int branchId = Integer.parseInt(request.getParameter("branch_id"));
        int departmentId = Integer.parseInt(request.getParameter("department_id"));

        if(StringUtils.isEmpty(userId) == true) {
        	messages.add("ユーザーIDを入力してください");
        }else if(!userId.matches("^[0-9a-zA-Z]{6,20}$")) {
        	messages.add("ユーザーIDは半角英数字で6文字以上20文字以下で入力してください");
        }
        if(StringUtils.isEmpty(password) == true) {
        	messages.add("パスワードを入力してください");
        }else if(!password.matches("^[ -/:-@\\[-~]{6,20}$")) {
        	messages.add("パスワードは半角英数字(記号含む)で6文字以上20文字以下で入力してください");
        }else if(!password.equals(passwordCheck)) {
        	messages.add("パスワードとパスワード(確認用)の入力内容が違います。同じ内容を入力してください");
        }
        if(StringUtils.isEmpty(userName) == true) {
        	messages.add("ユーザー名を入力してください");
        }else if(!userName.matches("^.{1,10}$")) {
        	messages.add("ユーザー名は10文字以下で入力してください");
        }
        if(branchId == 0) {
        	messages.add("支店を選択してください");
        }
        if(departmentId == 0) {
        	messages.add("部署・役職を選択してください");
        }

        if(messages.size() == 0) {
        	return true;
        }else {
        	return false;
        }
    }


}
