package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UesrManagementService;

@WebServlet(urlPatterns = { "/userManagement" })
public class UesrManagementServlet extends HttpServlet  {
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		List<User> users = new UesrManagementService().getUser();
		request.setAttribute("users", users);
		
		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		String loginUserId = request.getQueryString();
		if(loginUserId.equals("1") || loginUserId.equals("2")) {
			request.getRequestDispatcher("UserManagement.jsp").forward(request, response);
		}else {
			List<String> messages = new ArrayList<>();
			messages.add("現在ログインしているアカウントはユーザーを管理する権限が与えられていません。");
			//HttpSession session = request.getSession();
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("home").forward(request, response);
		}


	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	String editUserId = request.getParameter("id");


    	//System.out.println("UM:" + editUserId);

    	HttpSession session = request.getSession();
    	session.setAttribute("editUserId", editUserId);
    	response.sendRedirect("userEdit");


    }
}
