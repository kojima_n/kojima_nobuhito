package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("Login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String user_id = request.getParameter("user_id");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(user_id, password);

        HttpSession session = request.getSession();
        if (user != null) {
            session.setAttribute("loginUser", user);
            response.sendRedirect("home");
        } else {

            List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました。");
            messages.add("ユーザーID・パスワードのどちらか、もしくはその両方が間違っています。");
            messages.add("正しいものを入力しなおしてください");
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("login");
        }
    }

}
