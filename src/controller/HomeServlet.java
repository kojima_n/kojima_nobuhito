package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Post;
import service.HomeService;

@WebServlet(urlPatterns = { "/home" })
public class HomeServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		List<Post> posts = new HomeService().getPost();
		request.setAttribute("posts", posts);

		List<Comment> comments = new HomeService().getComment();
		request.setAttribute("comments", comments);

        request.getRequestDispatcher("Home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	List<String> messages = new ArrayList<>();

    	request.setCharacterEncoding("UTF-8");

    	HttpSession session = request.getSession();
        if(isValid(request, messages) == true) {
    		Comment comment = new Comment();
            comment.setComment_text(request.getParameter("comment_text"));
            comment.setUser_id(Integer.parseInt(request.getParameter("user_id")));
            comment.setPost_id(Integer.parseInt(request.getParameter("post_id")));


            new HomeService().register(comment);

            response.sendRedirect("home");
        }else {



        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("home");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String commentText = request.getParameter("comment_text");
        int userId = Integer.parseInt(request.getParameter("user_id"));

        if(StringUtils.isEmpty(commentText) == true) {
        	messages.add("コメントを入力してください");
        }else if(!commentText.matches("^.{1,500}$")) {
        	messages.add("件名は500文字以下で入力してください");
        }
        if(userId == 0) {
        	messages.add("ユーザー情報を確認できません");
        }

        if(messages.size() == 0) {
        	return true;
        }else {
        	return false;
        }
    }
}
