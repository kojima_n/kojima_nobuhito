package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.SuspendService;
import service.UserService;

@WebServlet(urlPatterns = { "/accountSuspend" })
public class AccountSuspendServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		String editUserId = request.getQueryString();
		//System.out.println(editUserId);

		User editUser = new UserService().getUser(Integer.parseInt(editUserId));


		if(editUser.getAccount_suspended() == 0) {
			new SuspendService().accountSuspend(Integer.parseInt(editUserId));
		}else {
			new SuspendService().accountRevival(Integer.parseInt(editUserId));
		}


		response.sendRedirect("userManagement");
    }

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

	}

}
