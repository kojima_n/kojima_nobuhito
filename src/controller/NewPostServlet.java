package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import service.NewPostService;

@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("NewPost.jsp").forward(request, response);
    }

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> messages = new ArrayList<>();

    	request.setCharacterEncoding("UTF-8");

    	HttpSession session = request.getSession();
        if(isValid(request, messages) == true) {
    		Post post = new Post();
            post.setSubject(request.getParameter("subject"));
            post.setPost_text(request.getParameter("post_text"));
            post.setCategory(request.getParameter("category"));
            post.setUser_id(Integer.parseInt(request.getParameter("user_id")));



            new NewPostService().register(post);

            response.sendRedirect("home");
        }else {

        	

        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("newPost");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String subject = request.getParameter("subject");
        String postText = request.getParameter("post_text");
        String category = request.getParameter("category");
        int userId = Integer.parseInt(request.getParameter("user_id"));

        if(StringUtils.isEmpty(subject) == true) {
        	messages.add("件名を入力してください");
        }else if(!subject.matches("^.{1,30}$")) {
        	messages.add("件名は30文字以下で入力してください");
        }
        if(StringUtils.isEmpty(category) == true) {
        	messages.add("カテゴリーを入力してください");
        }else if(!category.matches("^.{1,10}$")) {
        	messages.add("カテゴリーは10文字以下で入力してください");
        }
        if(StringUtils.isEmpty(postText) == true) {
        	messages.add("本文を入力してください");
        }else if(!postText.matches("^.{1,1000}$")) {
        	messages.add("本文は1000文字以下で入力してください");
        }
        if(userId == 0) {
        	messages.add("ユーザー情報を確認できません");
        }

        if(messages.size() == 0) {
        	return true;
        }else {
        	return false;
        }
    }
}
