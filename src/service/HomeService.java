package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.Post;
import dao.CommentDao;
import dao.SelectDao;

public class HomeService {
	public List<Post> getPost() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		SelectDao PostDao = new SelectDao();
    		List<Post> ret = PostDao.getPost(connection);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao CommentDao = new CommentDao();
			CommentDao.insert(connection, comment);

			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public List<Comment> getComment() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		SelectDao CommentDao = new SelectDao();
    		List<Comment> ret = CommentDao.getComment(connection);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
}
