package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.SuspendDao;

public class SuspendService {
	public void accountSuspend(int editUserId) {
		Connection connection = null;

		try {
			connection = getConnection();

			SuspendDao accountSuspendDao = new SuspendDao();
			accountSuspendDao.accountSuspend(connection, editUserId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void accountRevival(int editUserId) {
		Connection connection = null;

		try {
			connection = getConnection();

			SuspendDao accountSuspendDao = new SuspendDao();
			accountSuspendDao.accountRevival(connection, editUserId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
