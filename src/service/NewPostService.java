package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Post;
import dao.PostDao;

public class NewPostService {
	public void register(Post post) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao PostDao = new PostDao();
			PostDao.insert(connection, post);

			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
