package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.DeletDao;

public class DeletService {
	public void commentDelet(int commentId) {
		Connection connection = null;

		try {
			connection = getConnection();

			DeletDao commentDeletDao = new DeletDao();
			commentDeletDao.commentDelet(connection, commentId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void postDelet(int commentId) {
		Connection connection = null;

		try {
			connection = getConnection();

			DeletDao postDeletDao = new DeletDao();
			postDeletDao.postDelet(connection, commentId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
