package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import beans.Department;
import dao.SelectDao;

public class signupService {

    public List<Branch> getBranch() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		SelectDao branchDao = new SelectDao();
    		List<Branch> ret = branchDao.getBranch(connection);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public List<Department> getDepartment() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		SelectDao departmentDao = new SelectDao();
    		List<Department> ret = departmentDao.getDepartment(connection);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
}
