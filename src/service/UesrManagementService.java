package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.SelectDao;
import dao.UserDao;
import utils.CipherUtil;

public class UesrManagementService {
	public List<User> getUser() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		SelectDao userDao = new SelectDao();
    		List<User> ret = userDao.getUser(connection);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

	public void update(User user) {
		Connection connection = null;

		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void notPwUpdate(User user) {
		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.notPwUpdate(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
