package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class SuspendDao {
	public void accountSuspend(Connection connection, int editUserId) {

		PreparedStatement ps = null;
		try {
			String sql ="UPDATE users SET account_suspended = 1 WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, editUserId);

			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}


	public void accountRevival(Connection connection, int editUserId) {

		PreparedStatement ps = null;
		try {
			String sql ="UPDATE users SET account_suspended = 0 WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, editUserId);

			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

}
