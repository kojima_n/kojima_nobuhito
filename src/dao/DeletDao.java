package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class DeletDao {
	public void commentDelet(Connection connection, int commentId) {

		PreparedStatement ps = null;
		try {
			String sql ="UPDATE comments SET comment_delet = 1 WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentId);

			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void postDelet(Connection connection, int postId) {

		PreparedStatement ps = null;
		try {
			String sql ="UPDATE posts SET post_delet = 1 WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, postId);

			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}
}
