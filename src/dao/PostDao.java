package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Post;
import exception.SQLRuntimeException;

public class PostDao {
	public void insert(Connection connection, Post post) {
		PreparedStatement ps = null;
		try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("subject");
            sql.append(", post_text");
            sql.append(", category");
            sql.append(", user_id");
            sql.append(", post_delet");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(") VALUES (");
            sql.append("?"); // subjct
            sql.append(", ?"); // post_text
            sql.append(", ?"); // category
            sql.append(", ?"); // user_id
            sql.append(", 0"); // post_delet
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, post.getSubject());
            ps.setString(2, post.getPost_text());
            ps.setString(3, post.getCategory());
            ps.setInt(4, post.getUser_id());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}


}
