package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {
	public void insert(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("user_id");
            sql.append(", password");
            sql.append(", user_name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", account_suspended");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // user_id
            sql.append(", ?"); // password
            sql.append(", ?"); // user_name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // department_id
            sql.append(", ?"); // account_suspended
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getUser_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getUser_name());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getDepartment_id());
            ps.setInt(6, user.getAccount_suspended());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	public User getUser(Connection connection, String user_id,
	        String password) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users INNER JOIN branches ON users.branch_id = branches.id "
	        		+ "INNER JOIN departments ON users.department_id = departments.id WHERE user_id = ? AND password = ? "
	        		+ "AND account_suspended = 0";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, user_id);
	        ps.setString(2, password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String user_id = rs.getString("user_id");
	            String password = rs.getString("password");
	            String user_name = rs.getString("user_name");
	            int branch_id = Integer.parseInt(rs.getString("branch_id"));
	            String branch_name = rs.getString("branch_name");
	            int department_id = Integer.parseInt(rs.getString("department_id"));
	            String department_name = rs.getString("department_name");
	            Date created_at = rs.getDate("created_date");
	            Date updated_date = rs.getDate("updated_date");
	            int account_suspended = rs.getInt("account_suspended");

	            User user = new User();
	            user.setId(id);
	            user.setUser_id(user_id);
	            user.setPassword(password);
	            user.setUser_name(user_name);
	            user.setBranch_id(branch_id);
	            user.setBranch_name(branch_name);
	            user.setDepartment_id(department_id);
	            user.setDepartment_name(department_name);
	            user.setCreated_at(created_at);
	            user.setUpdated_at(updated_date);
	            user.setAccount_suspended(account_suspended);


	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public User getUser(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
            String sql = "SELECT * FROM users INNER JOIN branches ON users.branch_id = branches.id "
            		+ "INNER JOIN departments ON users.department_id = departments.id WHERE users.id = ?";

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if(userList.isEmpty() == true) {
            	return null;
            }else if(2 <= userList.size()) {
            	throw new IllegalStateException("2 <= userList.size()");
            }else {
            	return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	public void notPwUpdate(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			 sql.append("UPDATE users SET ");
	         sql.append("user_id = ?");
	         sql.append(", user_name = ?");
	         sql.append(", branch_id = ?");
	         sql.append(", department_id = ?");
	         sql.append(", updated_date = CURRENT_TIMESTAMP");
	         sql.append(" WHERE");
	         sql.append(" id = ?");

	         ps = connection.prepareStatement(sql.toString());

	         ps.setString(1, user.getUser_id());
	         ps.setString(2, user.getUser_name());
	         ps.setInt(3, user.getBranch_id());
	         ps.setInt(4, user.getDepartment_id());
	         ps.setInt(5, user.getId());

	         int count = ps.executeUpdate();
	         if(count == 0) {
	        	 throw new NoRowsUpdatedRuntimeException();
	         }
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			 sql.append("UPDATE users SET ");
	         sql.append("user_id = ?");
	         sql.append(", user_name = ?");
	         sql.append(", password = ?");
	         sql.append(", branch_id = ?");
	         sql.append(", department_id = ?");
	         sql.append(", updated_date = CURRENT_TIMESTAMP");
	         sql.append(" WHERE");
	         sql.append(" id = ?");

	         ps = connection.prepareStatement(sql.toString());

	         ps.setString(1, user.getUser_id());
	         ps.setString(2, user.getUser_name());
	         ps.setString(3, user.getPassword());
	         ps.setInt(4, user.getBranch_id());
	         ps.setInt(5, user.getDepartment_id());
	         ps.setInt(6, user.getId());

	         int count = ps.executeUpdate();
	         if(count == 0) {
	        	 throw new NoRowsUpdatedRuntimeException();
	         }
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}
}
