package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {
	public void insert(Connection connection, Comment comment) {
		PreparedStatement ps = null;
		try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("comment_text");
            sql.append(", user_id");
            sql.append(", post_id");
            sql.append(", comment_delet");
            sql.append(", created_at");
            sql.append(", update_at");
            sql.append(") VALUES (");
            sql.append("?"); // comment_text
            sql.append(", ?"); // user_id
            sql.append(", ?"); // post_id
            sql.append(", 0"); // comment_delet
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getComment_text());
            ps.setInt(2, comment.getUser_id());
            ps.setInt(3, comment.getPost_id());;
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

}
