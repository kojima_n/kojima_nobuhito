package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import beans.Comment;
import beans.Department;
import beans.Post;
import beans.User;
import exception.SQLRuntimeException;

public class SelectDao {
	public List<Branch> getBranch(Connection connection) {

		PreparedStatement ps = null;
		try {
		String sql = "SELECT * FROM branches";

		ps = connection.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		return toBranchList(rs);
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String branch_name = rs.getString("branch_name");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setBranch_name(branch_name);

				ret.add(branch);

			}
			return ret;
		}finally {
			close(rs);
		}
	}


	public List<Department> getDepartment(Connection connection) {

		PreparedStatement ps = null;
		try {
		String sql = "SELECT * FROM departments";

		ps = connection.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		return toDepartmentList(rs);
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<Department> toDepartmentList(ResultSet rs) throws SQLException {

		List<Department> ret = new ArrayList<Department>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String department_name = rs.getString("department_name");

				Department departmnet = new Department();
				departmnet.setId(id);
				departmnet.setDepartment_name(department_name);

				ret.add(departmnet);

			}
			return ret;
		}finally {
			close(rs);
		}
	}

	public List<User> getUser(Connection connection) {

		PreparedStatement ps = null;
		try {
		String sql = "SELECT * FROM users INNER JOIN branches ON users.branch_id = branches.id "
				+ "INNER JOIN departments ON users.department_id = departments.id";

		ps = connection.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		return toUserList(rs);
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
	            String user_id = rs.getString("user_id");
	            String password = rs.getString("password");
	            String user_name = rs.getString("user_name");
	            int branch_id = Integer.parseInt(rs.getString("branch_id"));
	            String branch_name = rs.getString("branch_name");
	            int department_id = Integer.parseInt(rs.getString("department_id"));
	            String department_name = rs.getString("department_name");
	            Date created_at = rs.getDate("created_date");
	            Date updated_date = rs.getDate("updated_date");
	            int account_suspended = rs.getInt("account_suspended");

	            User user = new User();
	            user.setId(id);
	            user.setUser_id(user_id);
	            user.setPassword(password);
	            user.setUser_name(user_name);
	            user.setBranch_id(branch_id);
	            user.setBranch_name(branch_name);
	            user.setDepartment_id(department_id);
	            user.setDepartment_name(department_name);
	            user.setCreated_at(created_at);
	            user.setUpdated_at(updated_date);
	            user.setAccount_suspended(account_suspended);

				ret.add(user);

			}
			return ret;
		}finally {
			close(rs);
		}
	}

	public List<Post> getPost(Connection connection) {

		PreparedStatement ps = null;
		try {
		String sql = "SELECT * FROM posts INNER JOIN users ON posts.user_id = users.id WHERE post_delet = 0";

		ps = connection.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		return toPostList(rs);
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	
	
	

	private List<Post> toPostList(ResultSet rs) throws SQLException {

		List<Post> ret = new ArrayList<>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String postText = rs.getString("post_text");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				String userName = rs.getString("user_name");
				Date createdAt = rs.getDate("created_at");
				int postDelet = rs.getInt("post_delet");


				Post post = new Post();
				post.setId(id);
				post.setSubject(subject);
				post.setPost_text(postText);
				post.setCategory(category);
				post.setUser_id(userId);
				post.setUser_name(userName);
				post.setCreated_at(createdAt);
				post.setPost_delet(postDelet);

				ret.add(post);

			}
			return ret;
		}finally {
			close(rs);
		}
	}

	public List<Comment> getComment(Connection connection) {

		PreparedStatement ps = null;
		try {
		String sql = "SELECT * FROM comments INNER JOIN users ON comments.user_id = users.id";

		ps = connection.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		return toCommentList(rs);
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<Comment> toCommentList(ResultSet rs) throws SQLException {

		List<Comment> ret = new ArrayList<>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String commentText = rs.getString("comment_text");
				int userId = rs.getInt("user_id");
				String userName = rs.getString("user_name");
				Date createdAt = rs.getDate("created_at");
				int postId = rs.getInt("post_id");
				int commentDelet = rs.getInt("comment_delet");


				Comment comment = new Comment();
				comment.setId(id);
				comment.setComment_text(commentText);
				comment.setUser_id(userId);
				comment.setUser_name(userName);
				comment.setCreated_at(createdAt);
				comment.setPost_id(postId);
				comment.setComment_delet(commentDelet);

				ret.add(comment);

			}
			return ret;
		}finally {
			close(rs);
		}
	}
}
