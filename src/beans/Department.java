package beans;

import java.io.Serializable;
import java.sql.Date;

public class Department implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String department_name;
	private Date crated_at;
	private Date updated_at;

	//getter,setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
	public Date getCrated_at() {
		return crated_at;
	}
	public void setCrated_at(Date crated_at) {
		this.crated_at = crated_at;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}



}
