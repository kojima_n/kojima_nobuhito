<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ログイン画面</title>
</head>
<body>
	<h1>ログイン</h1>
	<h3>ユーザーIDとパスワードを入力してください</h3>
	<c:if test="${not empty errorMessages}">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<form action="login" method="post">
	<div class="login">
	<br /> <label for="user_id">ユーザーID：</label> <input type="text" name="user_id" />
	<br /> <label for="password">パスワード：</label> <input type="password" name="password" />
	</div>
	<br /> <input type="submit" value="ログイン">
	</form>
	<h1>&emsp;</h1>
	<div class="copright">Copyright(c)kojima_nobuhito</div>
</body>
</html>