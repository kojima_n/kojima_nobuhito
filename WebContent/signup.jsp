<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link href="./css/style.css" rel="stylesheet" type="text/css">

<title>ユーザーの新規登録</title>
</head>
<body>
	<a href="userManagement?${loginUser.department_id}">戻る</a>
	<h1>ユーザーの新規登録</h1>
	<c:if test="${not empty errorMessages}">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<form action="signup" method="post">
	<div class="signup">
		<br /> <label for="user_id">ユーザーID：</label> <input type="text" name="user_id" />
		<br /> <label for="password">パスワード：</label> <input type="password" name="password" />
		<br /> <label for="password_chack">パスワード(確認用)：</label> <input type="password" name="password_chack" />
		<br /> <label for="user_name">ユーザー名：</label> <input type="text" name="user_name" />
		<br /> <label for="branch">支店名：</label>
		<select name="branch_id">
		 	<option value="0" selected>支店を選択してください</option>
			<c:forEach items="${branches}" var="branch">
				<option value="${branch.id}">"${branch.branch_name}"</option>
			</c:forEach>
		</select>
		<br /> <label for="department">部署・役職：</label>
		<select name="department_id">
			<option value="0" selected>部署・役職を選択してください</option>
			<c:forEach items="${departments}" var="department">
			<option value="${department.id}">"${department.department_name}"</option>
			</c:forEach>
		</select>
		<input type="hidden" name="account_suspended" value="0" />
		<br /> <input type="submit" value="登録">
		<h1>&emsp;</h1>
		<div class="copright">Copyright(c)kojima_nobuhito</div>
	</div>
	</form>
</body>
</html>