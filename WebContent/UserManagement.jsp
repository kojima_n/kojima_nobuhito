<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理</title>

<script type="text/javascript">
<!--

function disp(accountSuspend, id){
	if(accountSuspend==0){
			if(window.confirm('アカウントを停止します。よろしいですか')) {
				location.href = "accountSuspend?" + id;
		}
		else{
			window.alert('アカウントの停止はキャンセルされました')
		}
	}
	else{
		if(window.confirm('アカウントを復活します。よろしいですか')) {
			location.href = "accountSuspend?" + id;
	}

	else{
		window.alert('アカウントの復活はキャンセルされました')
	}
	}

}

// -->
</script>
</head>
<body>
	<a href="home">戻る</a>&emsp;<a href="signup">ユーザー新規登録</a>
	<h1>ユーザー管理</h1>
	<br /> <h3>ユーザー一覧</h3>
	<form action="userManagement" method=post >
		<table class="users">
			<tr><th>ユーザーID</th><th>ユーザー名</th><th>編集</th><th>アカウントの状態</th></tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<td>${user.user_id}</td>
					<td>${user.user_name}</td>
					<td><input type="button" name="stop" value="編集" onClick="location.href='userEdit?${user.id}'"></td>
					<c:if test="${user.account_suspended==0}">
						<td><input type="button" name="stop" value="停止" onClick="disp(0, ${user.id})"></td>
					</c:if>
					<c:if test="${user.account_suspended==1}">
						<td><input type="button" name="revival" value="復活" onClick="disp(1, ${user.id})"></td>
					</c:if>
				</tr>
			</c:forEach>
		</table>
	</form>
	<h1>&emsp;</h1>
	<div class="copright">Copyright(c)kojima_nobuhito</div>
</body>
</html>