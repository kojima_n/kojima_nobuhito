<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿画面</title>
</head>
<body>
	<a href="home">戻る</a>
	<h1>新しい投稿作成します</h1>
	<h2>下記の項目を入力してください</h2>
	<c:if test="${not empty errorMessages}">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<form action="newPost" method=post>
		<div class="newPost">
			<br /> <label for="subject">件名：</label> <input type="text" name="subject" />
			<br /> <label for="category">カテゴリー：</label> <input type="text" name="category" />
			<br /> <label for="post_text">本文：</label> <textarea  name="post_text" rows="4" cols="40 "></textarea>
			<br /> <label for="user_id">投稿者：<c:out value="${loginUser.user_name}" /></label>
				<input type="hidden" name="user_id" value="${loginUser.id}" />
		</div>
			<br /> <input type="submit" value="投稿">

	</form>

	<h1>&emsp;</h1>
	<div class="copright">Copyright(c)kojima_nobuhito</div>
</body>
</html>