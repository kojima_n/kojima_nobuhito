<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ホーム画面</title>
</head>
<body>
	<a href="logout">ログアウト</a>&emsp;<a href="newPost">新規投稿</a>&emsp;<a href="userManagement?${loginUser.department_id}">ユーザー管理</a>
	<h3>ログインユーザー:${loginUser.user_name}</h3>
	<h2>投稿検索</h2>
	<form action="narrowing" method="get">
		<br /> <label for="category">カテゴリーは</label> <input type="text" name="category" />で、
		<br /> <label for="post">投稿日は</label> <input type="date" name="minDate" id="minDate" /> ～ <input type="date" name="maxDate" id="maxDate" />
		で、<input type="submit" value="検索" />
	</form>

	<h2>投稿一覧</h2>
	<c:if test="${not empty errorMessages}">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<c:forEach items="${posts}" var="post">

		<br /> <label for="subuject">件名：<c:out value="${post.subject}" /></label>
			<label for="user_name">投稿者：<c:out value="${post.user_name}" /></label>
		<br /> <label for="category">カテゴリー：<c:out value="${post.category}" /></label>
			<label for="created_at">投稿日：<c:out value="${post.created_at}" /></label>
		<br /> <label for="post_text">本文：<c:out value="${post.post_text}" /></label>
		<c:if test="${post.user_id==loginUser.id}">
			<form action="postDelet" method=get>
				<input type="hidden" name="post_id" value="${post.id}" />
				<input type="submit" name="postDelet" value="投稿を削除" />
			</form>
		</c:if>

		<br />
		<hr style="border:none;border-top:dashed 1px ;" />
		<c:forEach items="${comments}" var="comment">
			<c:if test="${comment.post_id==post.id}">
				<c:if test="${comment.comment_delet==0}">
					<br /> <label for="user_name">投稿者：<c:out value="${comment.user_name}" /></label>
						<label for="created_at">投稿日：<c:out value="${comment.created_at}" /></label>
					<br /> <label for="comment_text">コメント：<c:out value="${comment.comment_text}" /></label>
					<c:if test="${comment.user_id==loginUser.id}">
						<form action="commentDelet" method=get>
							<input type="hidden" name="comment_id" value="${comment.id}" />
							<input type="submit" name="commentDelet" value="コメントを削除" />
						</form>
					</c:if>

					<hr />
				</c:if>
				<c:if test="${comment.comment_delet==1}">
				<br /> <label for="deletMessage">コメントは削除されました。</label>
				<hr />
				</c:if>
			</c:if>


		</c:forEach>

		<br />
		<form action="home" method=post>
			<br /> <label for="comment">コメント投稿欄：</label>
			<br /> <textarea name="comment_text" rows="4" cols="40" placeholder="ここにコメントを入力してください。"></textarea>
			<input type="hidden" name="user_id" value="${loginUser.id}" />
			<input type="hidden" name="post_id" value="${post.id}" />
			<br /> <input type="submit" name="comment_post" value="コメントを投稿" />
		</form>

		<hr style="border:none;border-top:double 3px ;"/>
	</c:forEach>

	<h1>&emsp;</h1>
	<div class="copright">Copyright(c)kojima_nobuhito</div>
</body>
</html>